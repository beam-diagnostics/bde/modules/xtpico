/*
 * AKI2C_LTC2991.cpp
 *
 *  Created on: May 24, 2016
 *      Author: hinkokocevar
 */


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>
#include <ctype.h>

#include <alarm.h>
#include <epicsTypes.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsTimer.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <epicsExit.h>
#include <epicsExport.h>
#include <iocsh.h>

#include <asynPortDriver.h>
#include "AKI2C_LTC2991.h"

static const char *driverName = "AKI2C_LTC2991";


static void exitHandler(void *drvPvt) {
	AKI2C_LTC2991 *pPvt = (AKI2C_LTC2991 *)drvPvt;
	pPvt->lock();
	delete pPvt;
}

asynStatus AKI2C_LTC2991::write(int addr, unsigned char reg, unsigned char val, unsigned short len) {
	asynStatus status = asynSuccess;
	unsigned char data[1] = {0};
	int devAddr;

	getIntegerParam(addr, AKI2CDevAddr, &devAddr);

	data[0] = val;
	status = xfer(addr, AK_REQ_TYPE_WRITE, devAddr, 1, data, &len, reg);
	if (status) {
		return status;
	}

	return status;
}

asynStatus AKI2C_LTC2991::writeTrigger(int addr, unsigned short val) {
	asynStatus status = asynSuccess;

	status = write(addr, AKI2C_LTC2991_CH_ENABLE_REG, val, 1);
	if (status) {
		return status;
	}

	return status;
}

void AKI2C_LTC2991::convertToSingleEndedVoltage(int addr, int valueParam,
		int offsetParam, int factorParam, unsigned int raw) {
	double volts = 0.0;
	double offset = 0.0;
	double factor = 1.0;
	int val = 0;

	if (offsetParam != -1) {
		getDoubleParam(addr, offsetParam, &offset);
	}
	if (factorParam != -1) {
		getDoubleParam(addr, factorParam, &factor);
	}

	setParamAlarmStatus(addr, valueParam, NO_ALARM);
	setParamAlarmSeverity(addr, valueParam, NO_ALARM);

	if (raw & 0x8000) {
		/* Data is valid */
		if (raw & 0x4000) {
			/* if bit 14 (SIGN) == 1 we have negative value */
			val = (raw & 0x3FFF) | ~((1 << 14) - 1);
		} else {
			val = (raw & 0x3FFF);
		}
		/* LSB = 305.18 uV ; 2.5V / 2^13 */
		volts = factor * ((double)val * 305.18 / 1000000.0) + offset;
		if (valueParam == AKI2C_LTC2991_Vcc_Value) {
			volts += 2.5;
		}
		setDoubleParam(addr, valueParam, volts);
	}
}

void AKI2C_LTC2991::convertToDifferentialVoltage(int addr, int valueParam,
		int offsetParam, int factorParam, unsigned int raw) {
	double volts = 0.0;
	double offset = 0.0;
	double factor = 1.0;
	int val = 0;

	if (offsetParam != -1) {
		getDoubleParam(addr, offsetParam, &offset);
	}
	if (factorParam != -1) {
		getDoubleParam(addr, factorParam, &factor);
	}

	setParamAlarmStatus(addr, valueParam, NO_ALARM);
	setParamAlarmSeverity(addr, valueParam, NO_ALARM);

	if (raw & 0x8000) {
		/* Data is valid */
		if (raw & 0x4000) {
			/* if bit 14 (SIGN) == 1 we have negative value */
			/* the equation is (neg(D[13:0]) + 1 ) * -19.075 */
			/* but we do this instead: D[13:0] - 1 * 19.075 */
			val = (raw & 0x3FFF) - 1;
		} else {
			val = (raw & 0x3FFF);
		}
		/* LSB = 19.075 uV ; 2.5V / 2^17 */
		volts = factor * ((double)val * 19.075 / 1000000.0) + offset;
		setDoubleParam(addr, valueParam, volts);
	}
}

void AKI2C_LTC2991::convertToTemperature(int addr, int valueParam,
		unsigned int raw) {
	double temp = 0.0;
	int val = 0;

	setParamAlarmStatus(addr, valueParam, NO_ALARM);
	setParamAlarmSeverity(addr, valueParam, NO_ALARM);

	if (raw & 0x8000) {
		/* Data is valid */
		if (raw & 0x1000) {
			/* if bit 12 == 1 we have negative value */
			val = (raw & 0x1FFF) | ~((1 << 13) - 1);
		} else {
			val = (raw & 0x1FFF);
		}

		/* LSB = 0.0625 degrees */
		temp = (double)val * 0.0625;
		setDoubleParam(addr, valueParam, temp);
	}
}

ChannelConfig AKI2C_LTC2991::controlRegToConfig(int regbits, bool oddChannel) {
	regbits &= 0x03;

	/* 1 = Temperature, 0 = Voltage (per b0 settings) */
	if (regbits & 0x02)
		return ConfigTemperature;

	/* 1 = Differential (V1 – V2) and V1 Single-Ended */
	/* 0 = Single-Ended Voltage (V1 and V2) */
	if (regbits & 0x01 && ! oddChannel)
		return ConfigDifferentialVoltage;

	return ConfigSingleEndedVoltage;
}

void AKI2C_LTC2991::setCommAlarms(int addr) {
	int params[] = {
		AKI2C_LTC2991_V1_Value,
		AKI2C_LTC2991_V2_Value,
		AKI2C_LTC2991_V3_Value,
		AKI2C_LTC2991_V4_Value,
		AKI2C_LTC2991_V5_Value,
		AKI2C_LTC2991_V6_Value,
		AKI2C_LTC2991_V7_Value,
		AKI2C_LTC2991_V8_Value,
		AKI2C_LTC2991_Vcc_Value,
		AKI2C_LTC2991_TInt_Value, };

	for (unsigned int i = 0; i < sizeof(params) / sizeof(params[0]); ++i) {
		setParamAlarmStatus(addr, params[i], COMM_ALARM);
		setParamAlarmSeverity(addr, params[i], INVALID_ALARM);
	}
}

asynStatus AKI2C_LTC2991::read(int addr, unsigned char reg, unsigned char *val, unsigned short len) {
	asynStatus status = asynSuccess;
	int devAddr;

	getIntegerParam(addr, AKI2CDevAddr, &devAddr);

	status = xfer(addr, AK_REQ_TYPE_READ, devAddr, 1, val, &len, reg);
	if (status) {
		return status;
	}

	return status;
}

asynStatus AKI2C_LTC2991::readAll(int addr) {
	asynStatus status = asynSuccess;
	unsigned char data[32] = {0};
	unsigned int raw;

	/* Read all 30 registers at once */
	status = read(addr, AKI2C_LTC2991_STATUS_LOW_REG, data, 30);
	if (status) {
		setCommAlarms(addr);
		return status;
	}

	/* Check for BUSY bit - if 0 conversion is not in progress. */
	if (data[AKI2C_LTC2991_STATUS_HIGH_REG] & 0x04) {
		return asynError;
	}

#define convertToX(addr, config, val, offset, factor, raw) do { \
switch (config) { \
	case ConfigTemperature: \
		convertToTemperature(addr, val, raw); \
		break; \
	case ConfigSingleEndedVoltage: \
		convertToSingleEndedVoltage(addr, val, offset, factor, raw); \
		break; \
	case ConfigDifferentialVoltage: \
		convertToDifferentialVoltage(addr, val, offset, factor, raw); \
		break; \
} \
} while (0)

	/* Parse out the values for V1 */
	raw = ((unsigned int)data[AKI2C_LTC2991_V1_MSB_REG] << 8)
			| (unsigned int)data[AKI2C_LTC2991_V1_LSB_REG];
	convertToX(addr, mV1_Config, AKI2C_LTC2991_V1_Value,
			AKI2C_LTC2991_V1_Offset, AKI2C_LTC2991_V1_Factor, raw);
	raw = ((unsigned int)data[AKI2C_LTC2991_V2_MSB_REG] << 8)
			| (unsigned int)data[AKI2C_LTC2991_V2_LSB_REG];
	convertToX(addr, mV2_Config, AKI2C_LTC2991_V2_Value,
			AKI2C_LTC2991_V2_Offset, AKI2C_LTC2991_V2_Factor, raw);
	raw = ((unsigned int)data[AKI2C_LTC2991_V3_MSB_REG] << 8)
			| (unsigned int)data[AKI2C_LTC2991_V3_LSB_REG];
	convertToX(addr, mV3_Config, AKI2C_LTC2991_V3_Value,
			AKI2C_LTC2991_V3_Offset, AKI2C_LTC2991_V3_Factor, raw);
	raw = ((unsigned int)data[AKI2C_LTC2991_V4_MSB_REG] << 8)
			| (unsigned int)data[AKI2C_LTC2991_V4_LSB_REG];
	convertToX(addr, mV4_Config, AKI2C_LTC2991_V4_Value,
			AKI2C_LTC2991_V4_Offset, AKI2C_LTC2991_V4_Factor, raw);
	raw = ((unsigned int)data[AKI2C_LTC2991_V5_MSB_REG] << 8)
			| (unsigned int)data[AKI2C_LTC2991_V5_LSB_REG];
	convertToX(addr, mV5_Config, AKI2C_LTC2991_V5_Value,
			AKI2C_LTC2991_V5_Offset, AKI2C_LTC2991_V5_Factor, raw);
	raw = ((unsigned int)data[AKI2C_LTC2991_V6_MSB_REG] << 8)
			| (unsigned int)data[AKI2C_LTC2991_V6_LSB_REG];
	convertToX(addr, mV6_Config, AKI2C_LTC2991_V6_Value,
			AKI2C_LTC2991_V6_Offset, AKI2C_LTC2991_V6_Factor, raw);
	raw = ((unsigned int)data[AKI2C_LTC2991_V7_MSB_REG] << 8)
			| (unsigned int)data[AKI2C_LTC2991_V7_LSB_REG];
	convertToX(addr, mV7_Config, AKI2C_LTC2991_V7_Value,
			AKI2C_LTC2991_V7_Offset, AKI2C_LTC2991_V7_Factor, raw);
	raw = ((unsigned int)data[AKI2C_LTC2991_V8_MSB_REG] << 8)
			| (unsigned int)data[AKI2C_LTC2991_V8_LSB_REG];
	convertToX(addr, mV8_Config, AKI2C_LTC2991_V8_Value,
			AKI2C_LTC2991_V8_Offset, AKI2C_LTC2991_V8_Factor, raw);
	raw = ((unsigned int)data[AKI2C_LTC2991_VCC_MSB_REG] << 8)
			| (unsigned int)data[AKI2C_LTC2991_VCC_LSB_REG];
	convertToSingleEndedVoltage(addr, AKI2C_LTC2991_Vcc_Value,
			-1, -1, raw);
	raw = ((unsigned int)data[AKI2C_LTC2991_TINT_MSB_REG] << 8)
			| (unsigned int)data[AKI2C_LTC2991_TINT_LSB_REG];
	convertToTemperature(addr, AKI2C_LTC2991_TInt_Value, raw);

#undef convertToX

	return status;
}

const char* AKI2C_LTC2991::strChannelConfig(ChannelConfig config) {
	switch (config) {
		case ConfigTemperature:
			return "Temperature";
		case ConfigSingleEndedVoltage:
			return "Single-ended";
		case ConfigDifferentialVoltage:
			return "Differential";
	}

	return "Unknown channel configuration";
}

asynStatus AKI2C_LTC2991::writeInt32(asynUser *pasynUser, epicsInt32 value) {

	int function = pasynUser->reason;
	int addr = 0;
	asynStatus status = asynSuccess;
	const char *functionName = "writeInt32";

	status = getAddress(pasynUser, &addr);
	if (status != asynSuccess) {
		return(status);
	}

	D(printf("function %d, addr %d, value %d\n", function, addr, value));
	status = setIntegerParam(addr, function, value);


	if (function == AKI2C_LTC2991_Read) {
		status = readAll(addr);
	} else if (function == AKI2C_LTC2991_Trigger) {
		/* The acquisition is triggered by writing to this register */
		status = writeTrigger(addr, AKI2C_LTC2991_CH_ENABLE_VAL);
	} else if (function < FIRST_AKI2C_LTC2991_PARAM) {
		/* If this parameter belongs to a base class call its method */
		status = AKI2C::writeInt32(pasynUser, value);
	}

	/* Do callbacks so higher layers see any changes */
	callParamCallbacks(addr, addr);

	if (status) {
		asynPrint(pasynUser, ASYN_TRACE_ERROR,
			"%s:%s: error, status=%s function=%d, addr=%d, value=%d\n",
			driverName, functionName, pasynManager->strStatus(status), function, addr, value);
	} else {
		asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
			"%s:%s: function=%d, addr=%d, value=%d\n",
			driverName, functionName, function, addr, value);
	}

	return status;
}

void AKI2C_LTC2991::report(FILE *fp, int details) {

	fprintf(fp, "AKI2C_LTC2991 %s\n", this->portName);
	if (details > 0) {
	}
	/* Invoke the base class method */
	AKI2C::report(fp, details);
}

/** Constructor for the AKI2C_LTC2991 class.
  * Calls constructor for the AKI2C base class.
  * All the arguments are simply passed to the AKI2C base class.
  */
AKI2C_LTC2991::AKI2C_LTC2991(const char *portName, const char *ipPort,
		int devCount, const char *devInfos, int priority, int stackSize,
		int ctrlReg1, int ctrlReg2, int ctrlReg3)
	: AKI2C(portName,
		ipPort,
		devCount, devInfos,
		0, /* no new interface masks beyond those in AKBase */
		0, /* no new interrupt masks beyond those in AKBase */
		ASYN_CANBLOCK | ASYN_MULTIDEVICE,
		1, /* autoConnect YES */
		priority, stackSize),
	mCtrlReg1(ctrlReg1),
	mCtrlReg2(ctrlReg2),
	mCtrlReg3(ctrlReg3)
{
	int status = asynSuccess;

	/* Create an EPICS exit handler */
	epicsAtExit(exitHandler, this);

	createParam(AKI2C_LTC2991_ReadString,		asynParamInt32,		&AKI2C_LTC2991_Read);
	createParam(AKI2C_LTC2991_TriggerString,	asynParamInt32,		&AKI2C_LTC2991_Trigger);
	createParam(AKI2C_LTC2991_V1ValueString,	asynParamFloat64,	&AKI2C_LTC2991_V1_Value);
	createParam(AKI2C_LTC2991_V1OffsetString,	asynParamFloat64,	&AKI2C_LTC2991_V1_Offset);
	createParam(AKI2C_LTC2991_V1FactorString,	asynParamFloat64,	&AKI2C_LTC2991_V1_Factor);
	createParam(AKI2C_LTC2991_V2ValueString,	asynParamFloat64,	&AKI2C_LTC2991_V2_Value);
	createParam(AKI2C_LTC2991_V2OffsetString,	asynParamFloat64,	&AKI2C_LTC2991_V2_Offset);
	createParam(AKI2C_LTC2991_V2FactorString,	asynParamFloat64,	&AKI2C_LTC2991_V2_Factor);
	createParam(AKI2C_LTC2991_V3ValueString,	asynParamFloat64,	&AKI2C_LTC2991_V3_Value);
	createParam(AKI2C_LTC2991_V3OffsetString,	asynParamFloat64,	&AKI2C_LTC2991_V3_Offset);
	createParam(AKI2C_LTC2991_V3FactorString,	asynParamFloat64,	&AKI2C_LTC2991_V3_Factor);
	createParam(AKI2C_LTC2991_V4ValueString,	asynParamFloat64,	&AKI2C_LTC2991_V4_Value);
	createParam(AKI2C_LTC2991_V4OffsetString,	asynParamFloat64,	&AKI2C_LTC2991_V4_Offset);
	createParam(AKI2C_LTC2991_V4FactorString,	asynParamFloat64,	&AKI2C_LTC2991_V4_Factor);
	createParam(AKI2C_LTC2991_V5ValueString,	asynParamFloat64,	&AKI2C_LTC2991_V5_Value);
	createParam(AKI2C_LTC2991_V5OffsetString,	asynParamFloat64,	&AKI2C_LTC2991_V5_Offset);
	createParam(AKI2C_LTC2991_V5FactorString,	asynParamFloat64,	&AKI2C_LTC2991_V5_Factor);
	createParam(AKI2C_LTC2991_V6ValueString,	asynParamFloat64,	&AKI2C_LTC2991_V6_Value);
	createParam(AKI2C_LTC2991_V6OffsetString,	asynParamFloat64,	&AKI2C_LTC2991_V6_Offset);
	createParam(AKI2C_LTC2991_V6FactorString,	asynParamFloat64,	&AKI2C_LTC2991_V6_Factor);
	createParam(AKI2C_LTC2991_V7ValueString,	asynParamFloat64,	&AKI2C_LTC2991_V7_Value);
	createParam(AKI2C_LTC2991_V7OffsetString,	asynParamFloat64,	&AKI2C_LTC2991_V7_Offset);
	createParam(AKI2C_LTC2991_V7FactorString,	asynParamFloat64,	&AKI2C_LTC2991_V7_Factor);
	createParam(AKI2C_LTC2991_V8ValueString,	asynParamFloat64,	&AKI2C_LTC2991_V8_Value);
	createParam(AKI2C_LTC2991_V8OffsetString,	asynParamFloat64,	&AKI2C_LTC2991_V8_Offset);
	createParam(AKI2C_LTC2991_V8FactorString,	asynParamFloat64,	&AKI2C_LTC2991_V8_Factor);
	createParam(AKI2C_LTC2991_VccValueString,	asynParamFloat64,	&AKI2C_LTC2991_Vcc_Value);
	createParam(AKI2C_LTC2991_VccOffsetString,	asynParamFloat64,	&AKI2C_LTC2991_Vcc_Offset);
	createParam(AKI2C_LTC2991_VccFactorString,	asynParamFloat64,	&AKI2C_LTC2991_Vcc_Factor);
	createParam(AKI2C_LTC2991_TIntValueString,	asynParamFloat64,	&AKI2C_LTC2991_TInt_Value);
	createParam(AKI2C_LTC2991_TIntOffsetString,	asynParamFloat64,	&AKI2C_LTC2991_TInt_Offset);
	createParam(AKI2C_LTC2991_TIntFactorString,	asynParamFloat64,	&AKI2C_LTC2991_TInt_Factor);

	for (int i = 0; i < devCount; i++) {
		setDoubleParam(i, AKI2C_LTC2991_V1_Offset, 0.0);
		setDoubleParam(i, AKI2C_LTC2991_V1_Factor, 1.0);
		setDoubleParam(i, AKI2C_LTC2991_V2_Offset, 0.0);
		setDoubleParam(i, AKI2C_LTC2991_V2_Factor, 1.0);
		setDoubleParam(i, AKI2C_LTC2991_V3_Offset, 0.0);
		setDoubleParam(i, AKI2C_LTC2991_V3_Factor, 1.0);
		setDoubleParam(i, AKI2C_LTC2991_V4_Offset, 0.0);
		setDoubleParam(i, AKI2C_LTC2991_V4_Factor, 1.0);
		setDoubleParam(i, AKI2C_LTC2991_V5_Offset, 0.0);
		setDoubleParam(i, AKI2C_LTC2991_V5_Factor, 1.0);
		setDoubleParam(i, AKI2C_LTC2991_V6_Offset, 0.0);
		setDoubleParam(i, AKI2C_LTC2991_V6_Factor, 1.0);
		setDoubleParam(i, AKI2C_LTC2991_V7_Offset, 0.0);
		setDoubleParam(i, AKI2C_LTC2991_V7_Factor, 1.0);
		setDoubleParam(i, AKI2C_LTC2991_V8_Offset, 0.0);
		setDoubleParam(i, AKI2C_LTC2991_V8_Factor, 1.0);
		setDoubleParam(i, AKI2C_LTC2991_Vcc_Offset, 0.0);
		setDoubleParam(i, AKI2C_LTC2991_Vcc_Factor, 1.0);
		setDoubleParam(i, AKI2C_LTC2991_TInt_Offset, 0.0);
		setDoubleParam(i, AKI2C_LTC2991_TInt_Factor, 1.0);

		/* Do callbacks so higher layers see any changes */
		callParamCallbacks(i, i);
	}

	/* set some defaults */
	for (int i = 0; i < devCount; i++) {
		status |= write(i, AKI2C_LTC2991_CONTROL1_REG, mCtrlReg1, 1);
		status |= write(i, AKI2C_LTC2991_CONTROL2_REG, mCtrlReg2, 1);
		status |= write(i, AKI2C_LTC2991_CONTROL3_REG, mCtrlReg3, 1);
	}

	mV1_Config = controlRegToConfig(mCtrlReg1, true);
	I(printf("V1 config: %s\n", strChannelConfig(mV1_Config)));
	mV2_Config = controlRegToConfig(mCtrlReg1, false);
	I(printf("V2 config: %s\n", strChannelConfig(mV2_Config)));
	mV3_Config = controlRegToConfig(mCtrlReg1 >> 4, true);
	I(printf("V3 config: %s\n", strChannelConfig(mV3_Config)));
	mV4_Config = controlRegToConfig(mCtrlReg1 >> 4, false);
	I(printf("V4 config: %s\n", strChannelConfig(mV4_Config)));

	mV5_Config = controlRegToConfig(mCtrlReg2, true);
	I(printf("V5 config: %s\n", strChannelConfig(mV5_Config)));
	mV6_Config = controlRegToConfig(mCtrlReg2, false);
	I(printf("V6 config: %s\n", strChannelConfig(mV6_Config)));
	mV7_Config = controlRegToConfig(mCtrlReg2 >> 4, true);
	I(printf("V7 config: %s\n", strChannelConfig(mV7_Config)));
	mV8_Config = controlRegToConfig(mCtrlReg2 >> 4, false);
	I(printf("V8 config: %s\n", strChannelConfig(mV8_Config)));

	if (status) {
		E(printf("failed to set parameter defaults!\n"));
		E(printf("init FAIL!\n"));
		disconnect(pasynUserSelf);
		return;
	}

	I(printf("init OK!\n"));
}

AKI2C_LTC2991::~AKI2C_LTC2991() {
	I(printf("shut down ..\n"));
}

/* Configuration routine.  Called directly, or from the iocsh function below */

extern "C" {

int AKI2CLTC2991Configure(const char *portName, const char *ipPort,
		int devCount, const char *devInfos, int priority, int stackSize,
		int ctrlReg1, int ctrlReg2, int ctrlReg3) {
	new AKI2C_LTC2991(portName, ipPort, devCount, devInfos, priority, stackSize, ctrlReg1, ctrlReg2, ctrlReg3);
	return(asynSuccess);
}

/* EPICS iocsh shell commands */

static const iocshArg initArg0 = { "portName",		iocshArgString};
static const iocshArg initArg1 = { "ipPort",		iocshArgString};
static const iocshArg initArg2 = { "devCount",		iocshArgInt};
static const iocshArg initArg3 = { "devInfos",		iocshArgString};
static const iocshArg initArg4 = { "priority",		iocshArgInt};
static const iocshArg initArg5 = { "stackSize",		iocshArgInt};
static const iocshArg initArg6 = { "ctrlReg1",		iocshArgInt};
static const iocshArg initArg7 = { "ctrlReg2",		iocshArgInt};
static const iocshArg initArg8 = { "ctrlReg3",		iocshArgInt};
static const iocshArg * const initArgs[] = {	&initArg0,
						&initArg1,
						&initArg2,
						&initArg3,
						&initArg4,
						&initArg5,
						&initArg6,
						&initArg7,
						&initArg8};
static const iocshFuncDef initFuncDef = {"AKI2CLTC2991Configure", sizeof(initArgs) / sizeof(initArgs[0]), initArgs};
static void initCallFunc(const iocshArgBuf *args) {
	AKI2CLTC2991Configure(args[0].sval, args[1].sval,
			args[2].ival, args[3].sval, args[4].ival, args[5].ival, args[6].ival, args[7].ival, args[8].ival);
}

void AKI2CLTC2991Register(void) {
	iocshRegister(&initFuncDef, initCallFunc);
}

epicsExportRegistrar(AKI2CLTC2991Register);

} /* extern "C" */
