/*
 * AKTTLIO.cpp
 *
 *  Created on: Feb 26, 2016
 *      Author: hinkokocevar
 */


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>
#include <ctype.h>

#include <epicsTypes.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsTimer.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <epicsExit.h>
#include <epicsExport.h>

#include <asynPortDriver.h>
#include "AKTTLIO.h"

static const char *driverName = "AKTTLIO";

asynStatus AKTTLIO::pack(unsigned char type, unsigned char *data,
		unsigned short len) {
	asynStatus status = asynSuccess;
	unsigned char msg[AK_MAX_MSG_SZ] = {0};
	int l = 0;

	// length is always two bytes
	if (len != 2) {
		sprintf(mStatusMsg, "Invalid TTL IO request length");
		return asynError;
	}

	// build the request
	msg[l++] = data[0];					// function
	msg[l++] = data[1];					// pin bit field

	memset(mReq, 0, AK_MAX_MSG_SZ);
	memset(mResp, 0, AK_MAX_MSG_SZ);
	mReqActSz = 0;
	mRespActSz = 0;
	mReqSz = l;
	// maximum bytes received:
	if (type == AK_REQ_TYPE_WRITE) {
		mRespSz = 0;
	} else if (type == AK_REQ_TYPE_READ) {
		mRespSz = 1;
	}

	memcpy(mReq, msg, mReqSz);

	return status;
}

asynStatus AKTTLIO::unpack(unsigned char type, unsigned char *data,
		unsigned short *len, asynStatus status) {

	if (mRespActSz == 1) {
		D(printf("OK! %02X\n", mResp[0] & 0xFF));
		if ((type == AK_REQ_TYPE_READ) && data && len) {
			memcpy(data, &mResp, *len * sizeof(unsigned char));
		}
		status = asynSuccess;
		sprintf(mStatusMsg, "OK");
	} else {
		sprintf(mStatusMsg, "Invalid TTL IO response size received");
	}

	return status;
}

asynStatus AKTTLIO::xfer(int asynAddr, unsigned char type, unsigned char *data,
		unsigned short *len, double timeout) {
	asynStatus status = asynSuccess;

	if ((type != AK_REQ_TYPE_WRITE) && (type != AK_REQ_TYPE_READ)) {
		sprintf(mStatusMsg, "Invalid request type");
		status = asynError;
	}

	if (status == asynSuccess) {
		status = pack(type, data, *len);
	}
	if (status == asynSuccess) {
		if (type == AK_REQ_TYPE_WRITE) {
			/* No data is returned by the remote host */
			status = ipPortWrite(timeout);
		} else {
			/* Single byte is returned by the remote host */
			status = ipPortWriteRead(timeout);
			status = unpack(type, data, len, status);
		}
	}

	if (status) {
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR,
			"%s::%s, status=%s, message=%s\n",
			driverName, __func__, pasynManager->strStatus(status), mStatusMsg);
	}

	setStringParam(asynAddr, AKStatusMessage, mStatusMsg);
	char m[AK_MAX_MSG_SZ] = {0};
	getStringParam(asynAddr, AKStatusMessage, AK_MAX_MSG_SZ, m);
	D(printf("Status message: '%s'\n", m));

	/* Do callbacks so higher layers see any changes */
	callParamCallbacks(asynAddr, asynAddr);

	/* return error status of the previous access */
	return status;
}

void AKTTLIO::report(FILE *fp, int details) {

	fprintf(fp, "AKTTLIO %s\n", this->portName);
	if (details > 0) {
	}
	/* Invoke the base class method */
	AKBase::report(fp, details);
}

/** Constructor for the AKTTLIO class.
  * Calls constructor for the AKBase base class.
  * All the arguments are simply passed to the AKBase base class.
  */
AKTTLIO::AKTTLIO(const char *portName, const char *ipPort,
		int interfaceMask, int interruptMask,
		int asynFlags, int autoConnect, int priority, int stackSize)
	: AKBase(portName,
		ipPort,
		AK_IP_PORT_TTLIO,
		1,
		interfaceMask,
		interruptMask,
		asynFlags, autoConnect, priority, stackSize)
{
	I(printf("init OK!\n"));
}

AKTTLIO::~AKTTLIO() {
	I(printf("shut down ..\n"));
}
