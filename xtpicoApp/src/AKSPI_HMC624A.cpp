/*
 * AKSPI_HMC624A.cpp
 *
 *  Created on: Nov 25, 2019
 *      Author: hinkokocevar
 */


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>
#include <ctype.h>

#include <epicsTypes.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsTimer.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <epicsExit.h>
#include <epicsExport.h>
#include <iocsh.h>

#include <asynPortDriver.h>
#include "AKSPI_HMC624A.h"

static const char *driverName = "AKSPI_HMC624A";


static void exitHandler(void *drvPvt) {
	AKSPI_HMC624A *pPvt = (AKSPI_HMC624A *)drvPvt;
	pPvt->lock();
	delete pPvt;
}

asynStatus AKSPI_HMC624A::setValue(int addr, double value) {
	asynStatus status = asynSuccess;
	unsigned char data[3] = {0};

	// Note: 3 bytes are sent because the SPI bus is configured to work
	//       with 24 bit messages!! Only low 6 bits of the last byte
	//       are used by the hmc624a part!
	unsigned short len = 3;
	// mapping of user vs. part value is as follows:
	// user : 0  .. 31.5 dB
	// part : 63 .. 0
	// Example:  0.0 dB attenuation --> write 63 to the part
	//          16.0 dB attenuation --> write 31 to the part
	//          31.5 dB attenuation --> write 0 to the part
	unsigned char val = (unsigned char)(value * 2);
	// last byte matters
	data[2] = ~(val) & 0x3F;
	// XXX workaround for the (broken?) firmware in XT pico that seems
	// to not send three bytes, but only two for the last request
	// AK-NORD contacted; remove this once firmware is fixed..
	data[0] = ~(val) & 0x3F;
	data[1] = ~(val) & 0x3F;
	I(printf("WRITE %f dB, val %d, inv val %d, hex msg %02X %02X %02X\n",
		value, val, data[2], data[0], data[1], data[2]));
	status = xfer(addr, AK_REQ_TYPE_WRITE, data, &len);
	if (status) {
		return status;
	}

	return status;
}

asynStatus AKSPI_HMC624A::writeFloat64(asynUser *pasynUser, epicsFloat64 value) {

	int function = pasynUser->reason;
	int addr = 0;
	asynStatus status = asynSuccess;
	const char *functionName = "writeFloat64";

	status = getAddress(pasynUser, &addr);
	if (status != asynSuccess) {
		return(status);
	}

	I(printf("function %d, addr %d, value %f\n", function, addr, value));
	status = setDoubleParam(addr, function, value);

	if (function == AKSPI_HMC624A_Value) {
		status = setValue(addr, value);
	} else if (function < FIRST_AKSPI_HMC624A_PARAM) {
		/* If this parameter belongs to a base class call its method */
		status = AKSPI::writeFloat64(pasynUser, value);
	}

	/* Do callbacks so higher layers see any changes */
	callParamCallbacks(addr, addr);

	if (status) {
		asynPrint(pasynUser, ASYN_TRACE_ERROR,
			"%s:%s: error, status=%s function=%d, addr=%d, value=%f\n",
			driverName, functionName, pasynManager->strStatus(status), function, addr, value);
	} else {
		asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
			"%s:%s: function=%d, addr=%d, value=%f\n",
			driverName, functionName, function, addr, value);
	}

	return status;
}

void AKSPI_HMC624A::report(FILE *fp, int details) {

	fprintf(fp, "AKSPI_HMC624A %s\n", this->portName);
	if (details > 0) {
	}
	/* Invoke the base class method */
	AKSPI::report(fp, details);
}

/** Constructor for the AKSPI_HMC624A class.
  * Calls constructor for the AKSPI base class.
  * All the arguments are simply passed to the AKSPI base class.
  */
AKSPI_HMC624A::AKSPI_HMC624A(const char *portName, const char *ipPort,
		int priority, int stackSize)
	: AKSPI(portName,
		ipPort,
		0, /* no new interface masks beyond those in AKBase */
		0, /* no new interrupt masks beyond those in AKBase */
		ASYN_CANBLOCK | ASYN_MULTIDEVICE,
		1, /* autoConnect YES */
		priority, stackSize)
{
	/* Create an EPICS exit handler */
	epicsAtExit(exitHandler, this);

	createParam(AKSPI_HMC624A_ValueString,	asynParamFloat64,	&AKSPI_HMC624A_Value);

	I(printf("init OK!\n"));
}

AKSPI_HMC624A::~AKSPI_HMC624A() {
	I(printf("shut down ..\n"));
}

/* Configuration routine.  Called directly, or from the iocsh function below */

extern "C" {

int AKSPIHMC624AConfigure(const char *portName, const char *ipPort,
		int priority, int stackSize) {
	new AKSPI_HMC624A(portName, ipPort, priority, stackSize);
	return(asynSuccess);
}

/* EPICS iocsh shell commands */

static const iocshArg initArg0 = { "portName",		iocshArgString};
static const iocshArg initArg1 = { "ipPort",		iocshArgString};
static const iocshArg initArg2 = { "priority",		iocshArgInt};
static const iocshArg initArg3 = { "stackSize",		iocshArgInt};
static const iocshArg * const initArgs[] = {&initArg0,
											&initArg1,
											&initArg2,
											&initArg3};
static const iocshFuncDef initFuncDef = {"AKSPIHMC624AConfigure", 4, initArgs};
static void initCallFunc(const iocshArgBuf *args) {
	AKSPIHMC624AConfigure(args[0].sval, args[1].sval,
			args[2].ival, args[3].ival);
}

void AKSPIHMC624ARegister(void) {
	iocshRegister(&initFuncDef, initCallFunc);
}

epicsExportRegistrar(AKSPIHMC624ARegister);

} /* extern "C" */
