/*
 * AKTTLIO_pins.cpp
 *
 *  Created on: Feb 26, 2016
 *      Author: hinkokocevar
 */


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>
#include <ctype.h>

#include <alarm.h>
#include <epicsTypes.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsTimer.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <epicsExit.h>
#include <epicsExport.h>
#include <iocsh.h>

#include <asynPortDriver.h>
#include "AKTTLIO_pins.h"

static const char *driverName = "AKTTLIO_pins";


static void exitHandler(void *drvPvt) {
	AKTTLIO_pins *pPvt = (AKTTLIO_pins *)drvPvt;
	pPvt->lock();
	delete pPvt;
}

asynStatus AKTTLIO_pins::write(int addr, unsigned char func, unsigned char val) {
	asynStatus status = asynSuccess;
	unsigned char data[2] = {0};
	unsigned short len;

	/* set function and pin bit field - hence len == 2 */
	data[0] = func & 0xff;
	data[1] = val & 0xff;
	len = 2;
	D(printf("func 0x%02d, WRITE pin bits 0x%02X\n", func, val));
	status = xfer(addr, AK_REQ_TYPE_WRITE, data, &len);
	if (status) {
		return status;
	}

	return status;
}

asynStatus AKTTLIO_pins::read(int addr, unsigned char func, unsigned char *val) {
	asynStatus status = asynSuccess;
	unsigned char data[2] = {0};
	unsigned short len;

	/* set function and pin bit field - hence len == 2 */
	data[0] = func & 0xff;
	data[1] = *val & 0xff;
	len = 2;
	status = xfer(addr, AK_REQ_TYPE_READ, data, &len);
	if (status) {
		return status;
	}

	*val = mResp[0] & 0xFF;
	D(printf("func 0x%02d, READ pin bits 0x%02X\n", func, *val));

	return status;
}

asynStatus AKTTLIO_pins::writeLevel(int addr, unsigned char param, unsigned char val) {
	asynStatus status = asynSuccess;
	unsigned char func;
	unsigned char bits;
	int pin;

	pin = param - AKTTLIO_LevelPin0;
	bits = 1 << pin;
	if (val == 0) {
		func = AKTTLIO_CLEAR_PINS_FUNC;
	} else {
		func = AKTTLIO_SET_PINS_FUNC;
	}

	status = write(addr, func, bits);
	if (status) {
		return status;
	}

	D(printf("param %d, pin %d, level %d\n", param, pin, val));

	return status;
}

asynStatus AKTTLIO_pins::readLevel(int addr, unsigned char param) {
	asynStatus status = asynSuccess;
	unsigned char bits;
	int pin;
	int val;

	pin = param - AKTTLIO_LevelPin0;
	bits = 1 << pin;
	status = read(addr, AKTTLIO_READ_PINS_FUNC, &bits);
	if (status) {
		setParamAlarmStatus(addr, param, COMM_ALARM);
		setParamAlarmSeverity(addr, param, INVALID_ALARM);
		return status;
	}

	val = (bits >> pin) & 1;
	setParamAlarmStatus(addr, param, NO_ALARM);
	setParamAlarmSeverity(addr, param, NO_ALARM);
	setIntegerParam(addr, param, val);
	D(printf("param %d, pin %d, level %d\n", param, pin, val));

	return status;
}

asynStatus AKTTLIO_pins::readLevelAll(int addr) {
	asynStatus status = asynSuccess;
	unsigned char bits;
	int param;
	int pin;
	int val;

	/* we want to read pin levels of all pins */
	bits = 0xFF;
	status = read(addr, AKTTLIO_READ_PINS_FUNC, &bits);
	if (status) {
		for (param = AKTTLIO_LevelPin0; param <= AKTTLIO_LevelPin7; ++param) {
			setParamAlarmStatus(addr, param, COMM_ALARM);
			setParamAlarmSeverity(addr, param, INVALID_ALARM);
		}
		return status;
	}

	for (param = AKTTLIO_LevelPin0; param <= AKTTLIO_LevelPin7; param++) {
		pin = param - AKTTLIO_LevelPin0;
		val = (bits >> pin) & 1;
		setParamAlarmStatus(addr, param, NO_ALARM);
		setParamAlarmSeverity(addr, param, NO_ALARM);
		setIntegerParam(addr, param, val);
		D(printf("param %d, pin %d, level %d\n", param, pin, val));
	}

	return status;
}

asynStatus AKTTLIO_pins::writeDirection(int addr, unsigned char param, unsigned char val) {
	asynStatus status = asynSuccess;
	unsigned char func;
	unsigned char bits;
	int pin;

	pin = param - AKTTLIO_DirPin0;
	bits = 1 << pin;
	if (val == 0) {
		func = AKTTLIO_OUTPUT_PINS_FUNC;
	} else {
		func = AKTTLIO_INPUT_PINS_FUNC;
	}

	status = write(addr, func, bits);
	if (status) {
		return status;
	}

	D(printf("param %d, pin %d, direction %d\n", param, pin, val));

	return status;
}

asynStatus AKTTLIO_pins::writePullUps(int addr, unsigned char param, unsigned char val) {
	asynStatus status = asynSuccess;
	unsigned char func;
	unsigned char bits;
	int pin;

	pin = param - AKTTLIO_PullUpPin0;
	bits = 1 << pin;
	if (val == 0) {
		func = AKTTLIO_NOPULLUP_PINS_FUNC;
	} else {
		func = AKTTLIO_PULLUP_PINS_FUNC;
	}

	status = write(addr, func, bits);
	if (status) {
		return status;
	}

	D(printf("param %d, pin %d, pullup %d\n", param, pin, val));

	return status;
}

asynStatus AKTTLIO_pins::writeInt32(asynUser *pasynUser, epicsInt32 value) {

	int function = pasynUser->reason;
	int addr = 0;
	asynStatus status = asynSuccess;
	const char *functionName = "writeInt32";

	status = getAddress(pasynUser, &addr);
	if (status != asynSuccess) {
		return(status);
	}

	D(printf("function %d, addr %d, value %d\n", function, addr, value));
	status = setIntegerParam(addr, function, value);

	if (function == AKTTLIO_Read) {
		status = readLevelAll(addr);
	} else if ((function >= AKTTLIO_LevelPin0) &&
			(function <= AKTTLIO_LevelPin7)) {
		status = writeLevel(addr, function, value);
	} else if ((function >= AKTTLIO_DirPin0) &&
			(function <= AKTTLIO_DirPin7)) {
		status = writeDirection(addr, function, value);
	} else if ((function >= AKTTLIO_PullUpPin0) &&
			(function <= AKTTLIO_PullUpPin7)) {
		status = writePullUps(addr, function, value);
	} else if (function < FIRST_AKTTLIO_PARAM) {
		/* If this parameter belongs to a base class call its method */
		status = AKTTLIO::writeInt32(pasynUser, value);
	}

	/* Do callbacks so higher layers see any changes */
	callParamCallbacks(addr, addr);

	if (status) {
		asynPrint(pasynUser, ASYN_TRACE_ERROR,
			"%s:%s: error, status=%s function=%d, addr=%d, value=%d\n",
			driverName, functionName, pasynManager->strStatus(status), function, addr, value);
	} else {
		asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
			"%s:%s: function=%d, addr=%d, value=%d\n",
			driverName, functionName, function, addr, value);
	}

	return status;
}

void AKTTLIO_pins::report(FILE *fp, int details) {

	fprintf(fp, "AKTTLIO_pins %s\n", this->portName);
	if (details > 0) {
	}
	/* Invoke the base class method */
	AKTTLIO::report(fp, details);
}

/** Constructor for the AKTTLIO_pins class.
  * Calls constructor for the AKTTLIO base class.
  * All the arguments are simply passed to the AKTTLIO base class.
  */
AKTTLIO_pins::AKTTLIO_pins(const char *portName, const char *ipPort,
		int priority, int stackSize)
	: AKTTLIO(portName,
		ipPort,
		0, /* no new interface masks beyond those in AKBase */
		0, /* no new interrupt masks beyond those in AKBase */
		ASYN_CANBLOCK | ASYN_MULTIDEVICE,
		1, /* autoConnect YES */
		priority, stackSize)
{
	int status = asynSuccess;

	/* Create an EPICS exit handler */
	epicsAtExit(exitHandler, this);

	createParam(AKTTLIO_ReadString,				asynParamInt32,	&AKTTLIO_Read);
	/* These need to be in sequence! */
	createParam(AKTTLIO_LevelPin0String,		asynParamInt32,	&AKTTLIO_LevelPin0);
	createParam(AKTTLIO_LevelPin1String,		asynParamInt32,	&AKTTLIO_LevelPin1);
	createParam(AKTTLIO_LevelPin2String,		asynParamInt32,	&AKTTLIO_LevelPin2);
	createParam(AKTTLIO_LevelPin3String,		asynParamInt32,	&AKTTLIO_LevelPin3);
	createParam(AKTTLIO_LevelPin4String,		asynParamInt32,	&AKTTLIO_LevelPin4);
	createParam(AKTTLIO_LevelPin5String,		asynParamInt32,	&AKTTLIO_LevelPin5);
	createParam(AKTTLIO_LevelPin6String,		asynParamInt32,	&AKTTLIO_LevelPin6);
	createParam(AKTTLIO_LevelPin7String,		asynParamInt32,	&AKTTLIO_LevelPin7);
	/* These need to be in sequence! */
	createParam(AKTTLIO_DirPin0String,			asynParamInt32,	&AKTTLIO_DirPin0);
	createParam(AKTTLIO_DirPin1String,			asynParamInt32,	&AKTTLIO_DirPin1);
	createParam(AKTTLIO_DirPin2String,			asynParamInt32,	&AKTTLIO_DirPin2);
	createParam(AKTTLIO_DirPin3String,			asynParamInt32,	&AKTTLIO_DirPin3);
	createParam(AKTTLIO_DirPin4String,			asynParamInt32,	&AKTTLIO_DirPin4);
	createParam(AKTTLIO_DirPin5String,			asynParamInt32,	&AKTTLIO_DirPin5);
	createParam(AKTTLIO_DirPin6String,			asynParamInt32,	&AKTTLIO_DirPin6);
	createParam(AKTTLIO_DirPin7String,			asynParamInt32,	&AKTTLIO_DirPin7);
	/* These need to be in sequence! */
	createParam(AKTTLIO_PullUpPin0String,		asynParamInt32,	&AKTTLIO_PullUpPin0);
	createParam(AKTTLIO_PullUpPin1String,		asynParamInt32,	&AKTTLIO_PullUpPin1);
	createParam(AKTTLIO_PullUpPin2String,		asynParamInt32,	&AKTTLIO_PullUpPin2);
	createParam(AKTTLIO_PullUpPin3String,		asynParamInt32,	&AKTTLIO_PullUpPin3);
	createParam(AKTTLIO_PullUpPin4String,		asynParamInt32,	&AKTTLIO_PullUpPin4);
	createParam(AKTTLIO_PullUpPin5String,		asynParamInt32,	&AKTTLIO_PullUpPin5);
	createParam(AKTTLIO_PullUpPin6String,		asynParamInt32,	&AKTTLIO_PullUpPin6);
	createParam(AKTTLIO_PullUpPin7String,		asynParamInt32,	&AKTTLIO_PullUpPin7);

	/* Fixme: Only single asyn address supported! */
	for (int i = 0; i < 1; i++) {

		/* XXX: Should we set some defaults?
		 *		Maybe it is better not to touch the
		 *		pin level at this point..
		 */

		/* read actual level from the ports */
		status |= readLevelAll(i);
		/* update port params */
		callParamCallbacks(i, i);
	}

	if (status) {
		E(printf("failed to set parameter defaults!\n"));
		E(printf("init FAIL!\n"));
		disconnect(pasynUserSelf);
		return;
	}

	I(printf("init OK!\n"));
}

AKTTLIO_pins::~AKTTLIO_pins() {
	I(printf("shut down ..\n"));
}

/* Configuration routine.  Called directly, or from the iocsh function below */

extern "C" {

int AKTTLIOConfigure(const char *portName, const char *ipPort,
		int priority, int stackSize) {
	new AKTTLIO_pins(portName, ipPort, priority, stackSize);
	return(asynSuccess);
}

/* EPICS iocsh shell commands */

static const iocshArg initArg0 = { "portName",		iocshArgString};
static const iocshArg initArg1 = { "ipPort",		iocshArgString};
static const iocshArg initArg2 = { "priority",		iocshArgInt};
static const iocshArg initArg3 = { "stackSize",		iocshArgInt};
static const iocshArg * const initArgs[] = {&initArg0,
											&initArg1,
											&initArg2,
											&initArg3};
static const iocshFuncDef initFuncDef = {"AKTTLIOConfigure", 4, initArgs};
static void initCallFunc(const iocshArgBuf *args) {
	AKTTLIOConfigure(args[0].sval, args[1].sval,
			args[2].ival, args[3].ival);
}

void AKITTLIORegister(void) {
	iocshRegister(&initFuncDef, initCallFunc);
}

epicsExportRegistrar(AKITTLIORegister);

} /* extern "C" */
