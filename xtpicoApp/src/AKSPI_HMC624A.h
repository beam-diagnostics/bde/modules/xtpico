/*
 * AKSPI_HMC624A.h
 *
 *  Created on: Jul 11, 2019
 *      Author: hinkokocevar
 */

#ifndef _AKSPI_HMC624A_H_
#define _AKSPI_HMC624A_H_


#include "AKSPI.h"

#define AKSPI_HMC624A_ValueString					"AKSPI_HMC624A_VALUE"

/*
 * Chip			: Analog Devices HMC624A
 * Function		: Digital Attenuator
 * Bus			: SPI
 * Access		: TCP/IP socket on AK-NORD XT-PICO-SX
 */
class AKSPI_HMC624A: public AKSPI {
public:
	AKSPI_HMC624A(const char *portName, const char *ipPort,
			int priority, int stackSize);
	virtual ~AKSPI_HMC624A();

	/* These are the methods that we override from AKSPI */
	virtual asynStatus writeFloat64(asynUser *pasynUser, epicsFloat64 value);
	void report(FILE *fp, int details);
	/* These are new methods */

protected:
	/* Our parameter list */
	int AKSPI_HMC624A_Value;
#define FIRST_AKSPI_HMC624A_PARAM AKSPI_HMC624A_Value

private:
	asynStatus setValue(int addr, double value);
};

#endif /* _AKSPI_HMC624A_H_ */
