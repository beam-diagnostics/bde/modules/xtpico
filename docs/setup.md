# EPICS IOC for AK-nord picoXT on ESS EthMod PCB

Author : Hinko kocevar <hinko.kocevar@esss.se>

Updated: 29 June 2018


We need to change the factory defaults of the individual pico XT module in order to be able to
use it on the ethernet module, and control it over EPICS. The module has a web interface over which
configuration can be performed.

Afterwards EPICS control over graphical user interface based on control system studio can be used.

Complete software stack has been installed into Ubuntu 16.04 VM and can be used as-is, provided that the host
machine can talk to picoXT module over Ethernet (static IP configured properly).

Complete process of getting the EthMod EPICS IOC and CSS to control it, is described below.

## settting up PC with proper network

In order to do this, both, pico XT and PC have to be on the same subnet.

NOTE: These actions need to be made on the host computer, not in the virtual machine with Ubuntu and IOC.
Host computer can be either Mac, Windows or any flavor of Linux as long as it runs VirtualBox 5.2.

Every pico XT comes preconfigured with static IP address 192.168.100.100 (netmask 255.255.0.0).
Use a direct (point to point) link between pico XT and PC to access the web interface on the pico XT.

Use your OS settings manager to configure the Network card IP as static, for example 192.168.100.1.
Verify that you can ping the pico XT at address 192.168.100.100.


## install VirtualBox

Download and install the VirtualBox 5.2 from here: https://www.virtualbox.org/wiki/Downloads.

## load ubuntu16.04 VM

Extract the zip with the ubuntu16.04 virtual machine into desired location.
Start the VirtualBox and select Machine -> Add .. .

![Add VM](setup-img/Screenshot_2018-06-29_14-00-41.png)

Point to 'ubuntu16.04.vbox' found in the folder where zip was extracted.

![Start VM](setup-img/Screenshot_2018-06-29_14-00-56.png)

## configure pico XT

NOTE: This actios can be performed either from host OS or VM ubuntu.

Open firefox and navigato to http://192.168.100.100.
Use username and password 'xt' to login.
You should see the following screen.

![Web UI main screen](setup-img/Screenshot_2018-06-29_13-50-42.png)

By typing a commang in the Input: field at the bottom of the web UI one can configure the pico XT.

Start by setting up the port 1 to be used for I2C as follows.

Type 'I' <ENTER> to enter the interfaces menu (I = INTERFACE MENU).

![Interface menu](setup-img/Screenshot_2018-06-29_14-11-56.png)

Type '1' <ENTER> to enter the port 1 menu (1 = SERIAL1 Menu).

![Port 1 menu](setup-img/Screenshot_2018-06-29_14-15-20.png)

Type '1' <ENTER> to enter the port 1 config menu (1 = SERIAL Config Menu).

![Port 1 config menu](setup-img/Screenshot_2018-06-29_14-17-04.png)

Type 'c=I2C' <ENTER> to use the port 1 as I2C port (c = BUS               = RS232). This will reconfigure the port 1
of the pico XT to act as I2C bus master.

![Port 1 config menu](setup-img/Screenshot_2018-06-29_14-17-04.png)

Now the BUS line should have changed to 'c = BUS               = I2C'.
In order to apply the changes, pico XT need to be restarted.
Go back to the main menu. by typing 'Q' <ENTER> until Web UI main screen is seen.

Type 'R' to restart the pico XT module and apply port 1 I2C setting.

After the device is back up, double check the port 1 settings by entering the same menus as above,
this time expecting the port 1 'c = BUS               = I2C' to be present.

![Port 1 as I2C](setup-img/Screenshot_2018-06-29_14-22-30.png)

This concludes the pico XT setup. From now it can be used on the Ethernet module PCB controlled by the EPICS IOC.

## starting the VM

Select the VM on left and click Start.

![Starting VM](setup-img/Screenshot_2018-06-29_14-25-54.png)

Ubuntu should start and login into the dev user session automatically.

On the VM desktop you can find two icons, for starting IOC and CS studio.

![VM desktop](setup-img/Screenshot_2018-06-29_14-26-51.png)

## start the IOC

Double click on the EthMod IOC icon to start the IOC in a terminal window.

![IOC started](setup-img/Screenshot_2018-06-29_14-28-31.png)

## start the CS studio

Double click on the ESS CS-Studio icon to start the user interface for the IOC.

![CSS started](setup-img/Screenshot_2018-06-29_14-30-23.png)

## controlling the I2C devices

Each device on the EthMod PCB has an associated button in the user interface.
Basic task is to perform data readout from the selected device.
For example for temperature sensors, click on the 'Read' button and the value should refresh.

![Read temperature](setup-img/Screenshot_2018-06-29_14-32-10.png)

Sometimes the communication with the I2C device times out and an error is printed in 'Status' box.
Retrying the operation results in success. This is a known issue with the pico XT at the moment; the
resolution of this issue is in progress at ESS.

![I2C error](setup-img/Screenshot_2018-06-29_14-32-27.png)

Clicking again on the 'Read' button read the temperature with success.

![I2C read OK](setup-img/Screenshot_2018-06-29_14-36-37.png)

